# Simple MySQL Backup for Debian/Ubuntu

This is a simple script to backup mysql or mariadb databases.

The script uses mysqldump to backup datbases on Debian or Ubuntu systems. It is intended to be used with a cron task for daily backup.

It comes with an install.sh script that copies the main script in /usr/local/sbin, the configuration file in /etc/ and create a cron task in /etc/cron.daily.

The configuration file simplemysqlbackup.conf is auto-documented and contains the parameters to exclude databases from backup, define the number of days to keep backups, define the backup folder and th ability to archive old backups.

## Français

C'est un script simple pour sauvegarder les bases de données mysql ou mariadb.

Le script utilise mysqldump pour sauvegarder les bases de données sur les systèmes Debian ou Ubuntu. Il est destiné à être utilisé avec une tâche cron pour la sauvegarde quotidienne.

Un script install.sh qui copie le script principal dans /usr/local/sbin, le fichier de configuration dans /etc/ et crée une tâche cron dans /etc/cron.daily, est fourni.

Le fichier de configuration simplemysqlbackup.conf est auto-documenté et contient les paramètres pour exclure les bases de données de la sauvegarde, définir le nombre de jours pour conserver les sauvegardes, définir le dossier de sauvegarde et la possibilité d'archiver les anciennes sauvegardes.
