#!/usr/bin/bash
# Backup script for mysql databases
#
#    VER. 0.9.1 - https://framagit.org/bruno666/simple-mysql-db-backup
#    Copyright (c) 2018-2020 bp-systems
#
#    SimpleMysqlBackup is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    SimpleMysqlBackup is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

get_backup_parameters() {

    backup_folder="/srv/mysql_backups"
    databases_exclude_list="information_schema performance_schema"
    compression_method='gzip'
    delete_backups_older_than_days=6
    archive_old_backups=true
    delete_archived_backup_older_than_days=90

    if [ -f /etc/simplemysqlbackup.conf ] ; then
        . /etc/simplemysqlbackup.conf
    fi

    if [ ! -d "$backup_folder" ] ; then
        $(command -v mkdir) -p "$backup_folder"
    fi

    compress_command=$(command -v $compression_method)
    file_suffix=".sql.${compression_method:0:2}"

}

get_databases_to_backup() {

    mysql_command=$(command -v mysql)
    databases_list=$($mysql_command --defaults-file=/etc/mysql/debian.cnf -Bse 'show databases')

    for exclude in $databases_exclude_list
    do
       databases_list=${databases_list//$exclude/}
    done

}
dump_databases () {

    mysqldump_command=$(command -v mysqldump)
    date_command=$(command -v date)
    current_date_iso8601=$($date_command +%FT%T)
    current_date_human_readable=$($date_command +%c)
    cd "$backup_folder" || exit

    report="-------------------------------------------------------\n SimpleMySQLBackup Report on ""$current_date_human_readable""\n-------------------------------------------------------\n Databases dumped in ""$backup_folder"" : \n\n"

    for database in $databases_list
    do
        if [ ! -d $database ] ; then
        $(command -v mkdir) -p $database
        fi
        file_to_save=$database/${database}_$current_date_iso8601$file_suffix
        $mysqldump_command --defaults-file=/etc/mysql/debian.cnf $database | $compress_command  > $file_to_save
        file_size=$($(command -v du) -h $file_to_save | $(command -v cut) -f 1)
        report+="\t""$file_to_save"" : ""$file_size""\n"
    done

    report+="-------------------------------------------------------\n"
}
delete_old_backups() {

    find_command=$(command -v find)
    rm_command=$(command -v rm)
    mv_command=$(command -v mv)

    report+=" Rotating backups : \n"

    for database in $databases_list
    do
        cd $backup_folder/$database || exit

        if [ $archive_old_backups = true ] ; then
            archives_folder="archives/"
            if [ ! -d $archives_folder ] ; then
                $(command -v mkdir) -p $archives_folder
            fi
            report+=$($find_command ./$archives_folder -daystart -mtime +$delete_archived_backup_older_than_days -type f -exec $rm_command -v {} \;)
            report+="\n"
            recent_archive=$($find_command ./$archives_folder -daystart -mtime -$((2 * delete_backups_older_than_days + 1)) -type f)
        fi

        if [ -z $recent_archive ] ; then
            report+=$($find_command ./ -maxdepth 1 -daystart -mtime +$delete_backups_older_than_days -type f -exec $mv_command -v {} $archives_folder \;)
        else
            report+=$($find_command ./ -maxdepth 1 -daystart -mtime +$delete_backups_older_than_days -type f -exec $rm_command -v {} \;)
        fi
    done

    report+="\n-------------------------------------------------------\n"

}

report () {
    printf '%b\n' "$report"
}

main() {
    unalias -a

    get_backup_parameters
    get_databases_to_backup
    dump_databases

    if [ $delete_backups_older_than_days != "-1" ] ; then
        delete_old_backups
    fi

    report

}

main
