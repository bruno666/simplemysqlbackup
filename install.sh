#!/bin/bash

unalias -a
command_cp=$(command -v cp)
command_chmod=$(command -v chmod)
user_id=$($(command -v id) -u)

copy_files () {
    $command_cp ./simplemysqlbackup.conf /etc/
    $command_cp ./simplemysqlbackup.sh /usr/local/sbin/
    $command_cp ./simplemysqlbackup_cron.sh /etc/cron.daily/simplemysqlbackup
    $command_chmod +x /usr/local/sbin/simplemysqlbackup.sh
    $command_chmod +x /etc/cron.daily/simplemysqlbackup
}
if [ $user_id != 0 ]; then
    echo "Install script must be run with root privileges"
    exit
else
    copy_files
    echo "Install finished"
fi
